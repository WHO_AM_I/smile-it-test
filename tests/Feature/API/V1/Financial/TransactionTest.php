<?php

namespace Tests\Feature\API\V1\Financial;

use App\Models\Financial\Account;
use App\Models\Financial\Transaction;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    use DatabaseTransactions;
    public function test_store_valid_transaction()
    {
        $balance = rand(10000, 99999);
        $amount = rand(1, 9999);
        $payer = Account::factory(['balance' => $balance])->create();
        $payee = Account::factory(['balance' => $balance])->create();
        $input = [
            'payer_account_id' => $payer['id'],
            'payee_account_id' => $payee['id'],
            'amount' => $amount,
        ];
        $response = $this->post(route('api.transactions.store'), $input);
        $response->assertStatus(201);
        $this->assertDatabaseHas('transactions', $input);

        $payer = $payer->refresh();
        $this->assertEquals($balance - $amount, $payer->balance);

        $payee = $payee->refresh();
        $this->assertEquals($balance + $amount, $payee->balance);
    }
    public function test_store_invalid_transaction_when_payer_doesnt_have_enough_balance()
    {
        $balance = rand(1000, 9999);
        $amount = rand(10000, 99999);
        $payer = Account::factory(['balance' => $balance])->create();
        $payee = Account::factory(['balance' => $balance])->create();

        $response = $this->post(route('api.transactions.store'), [
            'payer_account_id' => $payer['id'],
            'payee_account_id' => $payee['id'],
            'amount' => $amount,
        ], [
            'accept' => 'application/json'
        ]);
        $response->assertStatus(400);
    }
    public function test_store_invalid_transaction_with_incorrect_data()
    {
        $response = $this->post(route('api.transactions.store'), [
            'payer_account_id' => 80, /*does not exist*/
            'payee_account_id' => 81, /*does not exist*/
            'amount' => 'abcdefg',
        ], [
            'accept' => 'application/json'
        ]);
        $response->assertJsonValidationErrors([
            'payer_account_id',
            'payee_account_id',
            'amount',
        ]);
        $balance = rand(10000, 99999);
        $amount = rand(1, 9999);
        $payer = Account::factory(['balance' => $balance])->create();
        $payee = Account::factory(['balance' => $balance])->create();

        // amount should not be a lower than zero
        $response = $this->post(route('api.transactions.store'), [
            'payer_account_id' => $payer['id'],
            'payee_account_id' => $payee['id'],
            'amount' => -10000, // invalid amount value,
        ], [
            'accept' => 'application/json'
        ]);

        $response->assertJsonValidationErrors([
            'amount',
        ]);

        // payer and payee can not be same
        $response = $this->post(route('api.transactions.store'), [
            'payer_account_id' => $payer['id'],
            'payee_account_id' => $payer['id'],
            'amount' => $amount,
        ], [
            'accept' => 'application/json'
        ]);

        $response->assertJsonValidationErrors([
            'payee_account_id',
        ]);
    }
    public function test_account_history_transaction()
    {
        $balance = rand(1000, 100000);
        $amount = rand(10, 1000);
        $payer = Account::factory(['balance' => $balance])->create();
        $payee = Account::factory(['balance' => $balance])->create();
        Transaction::factory([
            'payer_account_id' => $payer['id'],
            'payee_account_id' => $payee['id'],
            'amount' => $amount
        ])->count(5)->create();

        Transaction::factory([
            'payer_account_id' => $payee['id'],
            'payee_account_id' => $payer['id'],
            'amount' => $amount
        ])->count(5)->create();

        $response = $this->get(route('api.transactions.history', ['account' => $payer]));
        $response->assertStatus(200);

        $account = Account::query()->find($payer['id']);
        $response->assertStatus(200);
    }
}
