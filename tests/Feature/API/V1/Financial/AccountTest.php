<?php

namespace Tests\Feature\API\V1\Financial;

use App\Models\Financial\Account;
use App\Models\User\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AccountTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_account_store_with_valid_data()
    {
        $user = User::factory()->create();
        $input = [
            'user_id' => $user['id'],
            'balance' => rand(1, 99999),
        ];
        $response = $this->post(route('api.accounts.store'), $input);
        $response->assertStatus(201);
        $this->assertDatabaseHas('accounts', $input);
    }
    public function test_user_account_store_with_invalid_data()
    {
        $response = $this->post(route('api.accounts.store'), [
            'user_id' => 80, /*does not exist*/
            'balance' => 'abcdefg'
        ], [
            'accept' => 'application/json'
        ]);
        $response->assertJsonValidationErrors([
            'user_id',
            'balance'
        ]);
        $response = $this->post(route('api.accounts.store'), [
            'user_id' => 80, /*does not exist*/
            'balance' => -1000 /*can not fill fewer than zero*/,
        ], [
            'accept' => 'application/json'
        ]);
        $response->assertJsonValidationErrors([
            'user_id',
            'balance'
        ]);
    }
    public function test_account_show()
    {
        $account = Account::factory()->create();
        $response = $this->get(route('api.accounts.show', ['account' => $account['id']]));
        $response->assertStatus(200);
    }
}
