<?php

namespace Database\Factories\Financial;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class AccountFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create()->id,
            'balance' => $this->faker->randomFloat()
        ];
    }
}
