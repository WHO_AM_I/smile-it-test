<?php

namespace Database\Factories\Financial;

use App\Models\Financial\Account;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'payer_account_id' => Account::factory()->create()->id,
            'payee_account_id' => Account::factory()->create()->id,
            'amount' => $this->faker->randomFloat()
        ];
    }
}
