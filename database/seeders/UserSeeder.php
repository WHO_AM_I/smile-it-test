<?php

namespace Database\Seeders;

use App\Models\User\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                "name" => "Arisha Barron"
            ],
            [
                "name" => "Branden Gibson"
            ],
            [
                "name" => "Rhonda Church"
            ],
            [
                "name" => "Georgina Hazel"
            ]
        ];
        User::insert($users);
    }
}
