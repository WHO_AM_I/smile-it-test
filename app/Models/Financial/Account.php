<?php

namespace App\Models\Financial;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'balance'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactions()
    {
        return Transaction::where('payer_account_id', $this->id)
            ->orWhere('payee_account_id', $this->id)
            ->orderBy('created_at', 'DESC')
            ->get();
    }
}
