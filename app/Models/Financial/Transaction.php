<?php

namespace App\Models\Financial;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $fillable = [
        'payer_account_id',
        'payee_account_id',
        'amount',
    ];

    public function account()
    {
        return $this->belongsTo(Account::class, 'payer_account_id');
    }
}
