<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function successResponse($message, $data = null, $code = 200): \Illuminate\Http\JsonResponse
    {
        $res = [
            'status' => true,
            'message' => __("messages.{$message}"),
            'data' => $data
        ];
        if (empty($data)) {
            unset($res['data']);
        }
        return response()
            ->json($res, $code);
    }
    protected function errorResponse($message, $error, $code = 400): \Illuminate\Http\JsonResponse
    {
        return response()
            ->json([
                'status' => false,
                'message' => __("errors.{$message}"),
                'error' => $error
            ], $code);
    }
}
