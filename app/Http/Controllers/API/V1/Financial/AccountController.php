<?php

namespace App\Http\Controllers\API\V1\Financial;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Financial\StoreAccountRequest;
use App\Models\Financial\Account;
use Illuminate\Http\Request;
use function Symfony\Component\Translation\t;

class AccountController extends Controller
{
    public function show(Account $account)
    {
        $account->load('user');
        return $this->successResponse(200, $account, 200);
    }

    public function store(StoreAccountRequest $request)
    {
        $data = $request->validated();
        $account = Account::create($data);
        if ($account) {
            $account->load('user');
            // we can use Resource here also
            return $this->successResponse(201, $account, 201);
        }
        return $this->errorResponse(400, __('errors.try_again'), 400);

    }
}
