<?php

namespace App\Http\Controllers\API\V1\Financial;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Financial\StoreTransactionRequest;
use App\Http\Resources\API\V1\Financial\TransactionHistoryResource;
use App\Models\Financial\Account;
use App\Models\Financial\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function history(Account $account)
    {
        request()->request->add(['account_id' => $account['id']]);
        $result = [
            'transactions' => TransactionHistoryResource::collection($account->transactions()),
            'account_balance' => $account['balance']
        ];
        return $this->successResponse(200, $result, 200);
    }

    public function store(StoreTransactionRequest $request)
    {
        $data = $request->validated();
        $payerAccount = Account::find($data['payer_account_id']);
        if ($payerAccount['balance'] < $data['amount']) {
            return $this->errorResponse(400, __('errors.balance_bot_enough'), 400);
        }
        $payeeAccount = Account::find($data['payee_account_id']);
        DB::beginTransaction();
        try {
            $payerAccount->decrement('balance', $data['amount']);
            $payeeAccount->increment('balance', $data['amount']);
            $transaction = Transaction::create($data);
            DB::commit();
            return $this->successResponse(201, $transaction, 201);
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->errorResponse(400, __('errors.try_again'), 400);
        }

    }
}
