<?php

namespace App\Http\Requests\API\V1\Financial;

use Illuminate\Foundation\Http\FormRequest;

class StoreTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'payer_account_id' => 'required|integer|min:1|exists:accounts,id',
            'payee_account_id' => 'required|integer|min:1|exists:accounts,id|different:payer_account_id',
            'amount' => 'required|numeric|min:1'

        ];
    }
}
