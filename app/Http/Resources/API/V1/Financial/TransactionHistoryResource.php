<?php

namespace App\Http\Resources\API\V1\Financial;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

    public function toArray($request)
    {
        $data = parent::toArray($request);
        return [
            'payer_account_id' => $data['payer_account_id'],
            'payee_account_id' => $data['payee_account_id'],
            'amount' => $data['amount'],
            'type' => $data['payer_account_id'] == request('account_id') ? 'outcome' : 'income',
            'transfer_date' => $data['created_at']
        ];
    }
}
