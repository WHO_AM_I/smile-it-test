<?php

use App\Http\Controllers\API\V1\Financial\AccountController;
use App\Http\Controllers\API\V1\Financial\TransactionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'financial'], function () {
        Route::group(['prefix' => 'accounts'], function () {
            Route::get('/{account}', [AccountController::class, 'show'])->name('api.accounts.show');
            Route::post('/', [AccountController::class, 'store'])->name('api.accounts.store');

        });
        Route::group(['prefix' => 'transactions'], function () {
            Route::get('/history/{account}', [TransactionController::class, 'history'])->name('api.transactions.history');
            Route::post('/', [TransactionController::class, 'store'])->name('api.transactions.store');
        });
    });
});


